## MAX32660 ring test
The fastest ring test I have been able to achieve using the MAX32660 is 1.61 MHz, using direct manipulation of the [unpublished](https://www.element14.com/community/roadTests/2005/l/ultra-low-power-arm-cortex-m4-darwin-mcu-evm) GPIO set and clear registers:

![ring test 4](ringtest4.png)
_[ignore the mean, min, max, and std dev values as they span several tests]_

Using Maxim's published GPIO manipulation registers, the speed drops to 1.10 MHz:

![ring test](ringtest.png)

This falls short of [Will's result](https://gitlab.cba.mit.edu/assembledAssemblers/assembledCircuits/blob/master/platforms/max32660/maximWorkflow.md) of 3.21 MHz. I checked the Clock Control Register (GCR_CLK_CTRL) and confirmed the system clock is running at 96 MHz with no scaling. I also confirmed the core voltage to be 1.1 VDC, which suggests the high speed oscillator is running at full speed.

I tried copying Will's fast code exactly and got a different result than him, 1.51 MHz:

![ring test 2](ringtest2.png)

For reference, here is his fastest ring test trace; note his waveform also appears closer to 50% duty cycle:

![ring test 3](ringtest3.jpg)

Finally, I tried directly toggling (i.e. not reading) a GPIO pin using both the published and the unpublished registers, and in both cases saw 3.45 MHz on the pin:

![ring test 5](ringtest5.png)

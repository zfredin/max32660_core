## MAX32660 minimal breadboard prototyping environment
This project includes the bare minimum needed to get started with the [MAX32660 Evaluation System](https://www.maximintegrated.com/en/products/microcontrollers/MAX32660-EVSYS.html), without relying on mBed, Keil, Windows, or any other service that requires your email address or wallet. The instructions assume a Linux environment; I'm using Mint 19 (based on Ubuntu 18.04).
### Requirements
You need the [ARM GNU toolchain](https://launchpad.net/gcc-arm-embedded):

    sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
    sudo apt-get update
    sudo apt-get install gcc-arm-none-eabi

You will also need various common Linux tools like `make`.
### Getting started
Clone the project repository to your local computer:

    git clone https://gitlab.cba.mit.edu/zfredin/max32660_core.git

Modify main.c as required. Right now it's got a simple LED flasher that uses the [per Will] inefficient Maxim GPIO drivers. I'll update this at some point to default to a ring test.

Navigate to this directory and run `make`. If you have all the prerequisites installed, a `build` directory should appear that contains a bunch of files. Navigate to this directory and run `arm-none-eabi-objcopy -O binary max32660.elf max32660.bin` to generate a binary file. Note that the `build` directory is in the `.gitignore` file so it won't show up in GitLab.

Connect the MAX32660 Evaluation System using a micro-USB cable. The board will enumerate as a mass storage device. Drag `max32660.bin` to the new drive; the LED on the programmer board (the one with the USB port) will flash a bit. When it stops, press the small black reset button the programmer board to run your program.

Run `make clean` from this directory (not the build directory) to erase the old compiled files and start fresh.

### Serial port
The MAX32660-EVSYS DAPLink (the programmer board) includes a built-in virtual serial port. To use it, make sure to include `#include "board.h"`, then simply use `printf` as normal. Connect to the detected serial port (/dev/ttyACM0 on Linux) using your serial terminal program of choice set to 115200 baud.

Note that `printf` is, in the realm of tiny microcontrollers, quite computationally expensive. Keep debug statements far away from time-critical parts of your code.

### Next Steps
If we end up continuing work with the MAX32660, I want to pull together a memory map and associated config files so we can flash/debug using the [Black Magic Probe](https://github.com/blacksphere/blackmagic). These devices are open-source (I respun the PCB to build one into my Thinkpad) which will make building everything into our automated assembler much easier; they also run a GDB server on the programmer itself so high-feature debugging (breakpoints, etc) are easy. But it's only worth making the time/effort investment if we settle on this chipset.

### Troubleshooting
zach.fredin@cba.mit.edu

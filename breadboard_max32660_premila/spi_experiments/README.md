## MAX32660 SPI experiments
I started with the SPI_Slave example which sends data out one SPI port and reads it back through the other. The original code runs at 1 MHz; I tried bumping the clock up and maxed out at 24 MHz:

![SPI experiments 1](spi_experiments1.png)
_[triggering off ch1, 6-pulse sequence]_

Setting the SPI clock speed above this value resulted in extremely slow (hundreds of Hz) values despite the MAX32660's advertised 48 MHz maximum SPI data rate. I suspect the low quality waveform is a result of the ~30 cm jumper wires used to connect the two SPI ports.

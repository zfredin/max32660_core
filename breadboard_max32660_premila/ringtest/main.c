/*
    MAX32660 ring test, using P0.2 and P0.3. Don't forget to jumper them together.
    Done using direct register manipulation for learning.
    Zach Fredin, 2019
*/

#include <stdio.h>
#include <stdint.h>
#include "gpio.h"

// shortcut for viewing/modifying an address in memory
#define poke_register(addr)     (*(volatile uint32_t *)(addr))

gpio_cfg_t output_pin;
gpio_cfg_t input_pin;

int main(void)
{
    // these are Maxim's GPIO setup functions. probably worth continuing to use
    output_pin.port = PORT_0;
    output_pin.mask = PIN_2;
    output_pin.pad = GPIO_PAD_NONE;
    output_pin.func = GPIO_FUNC_OUT;
    GPIO_Config(&output_pin);

    input_pin.port = PORT_0;
    input_pin.mask = PIN_3;
    input_pin.pad = GPIO_PAD_NONE;
    input_pin.func = GPIO_FUNC_IN;
    GPIO_Config(&input_pin);

    int toggle = 0; //used for simple toggling rather than a ring test

    while(1) {
        /*  0x40008000 = GPIO0 base address
            0x0018 = GPIO output register offset (from user guide)
            0x0024 = GPIO input register offset (from user guide)
            0x0020 = GPIO clear register offset (undocumented!)
            0x001c = GPIO set regsiter offset (undocumented!)
            GPIO pin numbers map 1:1 with register bits */


        //this runs at 1.10 MHz using the official GPIO control registers
        /*
        if (poke_register(0x40008000 + 0x0024) & (1<<3)) {
            poke_register(0x40008000 + 0x0018) &= ~(1<<2);
        }
        else {
            poke_register(0x40008000 + 0x0018) |= (1<<2);
        }
        */

        //this runs at 1.61 MHz using the undocumented GPIO set/clear registers

        if (poke_register(0x40008000 + 0x0024) & (1<<3)) {
            poke_register(0x40008000 + 0x0020) |= (1<<2);
        }
        else {
            poke_register(0x40008000 + 0x001c) |= (1<<2);
        }


        //this runs at 3.45 MHz using the official GPIO control regsiters, but isn't a ring test
        /*
        if (toggle == 0) {
            poke_register(0x40008000 + 0x0018) &= ~(1<<2);
            toggle = 1;
        }
        else {
            poke_register(0x40008000 + 0x0018) |= (1<<2);
            toggle = 0;
        }
        */

        //this runs at 3.45 MHz using the undocumented GPIO set/clear registers, but isn't a ring test
        /*if (toggle == 0) {
            poke_register(0x40008000 + 0x001c) |= (1<<2);
            toggle = 1;
        }
        else {
            poke_register(0x40008000 + 0x0020) |= (1<<2);
            toggle = 0;
        }
        */
    }
}
